## Qover App

This application is a full-stack demo application for Qover start-up using the following stack:

- React
- Redux
- Nodejs
- SCSS


To start the application in your terminal:

- `git clone`
- `yarn`
- `yarn start`

In another terminal to start the server:

`node index.js`

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


The creator of this project Miriam-Z

For any questions about this project contact:

miriam_z@icloud.com