import React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

import WelcomeComponent from './components/welcome/welcome.component.js';
import QuoteComponent from './components/quote/quote.component';
import PlanComponent from './components/plan/plan.component';
import store from './store'

function App() {
  return (
      <Provider store={store}>
      <Router>
          <div>
              <Switch>
                  <Route exact path={"/"} component={WelcomeComponent}/>
                  <Route path={"/quote"} component={QuoteComponent}/>
                  <Route path={"/plan"} component={PlanComponent}/>
              </Switch>
          </div>
      </Router>
      </Provider>
  );
}

export default App;
