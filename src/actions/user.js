import actionTypes from './';

export const login = (username) => {
    return {
        type: actionTypes.LOGIN_SUCCESS,
        payload: username
    }
}

export const logout = () => {
    return {
        type: actionTypes.LOGOUT
    }
}

