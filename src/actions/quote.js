import actionTypes from './';

export const calculateQuote = (quote) => {
    return {
        type: actionTypes.CAL_QUOTE_SUCCESS,
        payload: quote
    }
}