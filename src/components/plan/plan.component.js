import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import styles from './plan.component.scss';
import checkIcon from '../../images/baseline-check_circle-24px.svg'

class PlanComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
           year: true
        };
    }

  selectPlan = (plan) => {
        this.setState({
            plan
        })
  }

    handleChange = (e) => {
        const { name, checked } = e.target;
        this.setState({ [name]: checked });
    }

    render(){
        const { quote, history } = this.props;
        if(!quote){
            history.push("/quote")
        }
        const { plan, year } = this.state
        return (<div>
            <div className="switch">
                <label>
                    Yearly
                    <input type="checkbox" name="year" onChange={this.handleChange}/>
                        <span className="lever"></span>
                        Monthly
                </label>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col s6">
                        <h3>Global</h3>
                        <h1>{year? quote.globalPrice: (quote.globalPrice/12).toFixed(3)} €</h1>
                        <div>YEARLY INC. TAXES</div>
                        <div>Maximum duration travel of 90 days</div>
                        <div>Medical expenses reimbursement to to 1,000,000</div>
                        <div>Personal assistance abroad up to 5,000</div>
                        <div>Travel assistance abroad up to 1,000 per insured per travel</div>
                        <div>Coverage duration: 1 year</div>
                        <button onClick={() => this.selectPlan("global")}>{ plan === "global"?<Fragment> <img src={checkIcon} /> <span>Plan Selected</span> </Fragment>: "Choose Plan"}</button>

                    </div>
                    <div className="col s6">
                        <h3>Universe</h3>
                        <h1>{year? quote.universalPrice: (quote.universalPrice/12).toFixed(3)} €</h1>
                        <div>YEARLY INC. TAXES</div>
                        <div>Maximum duration travel of 180 days</div>
                        <div>Medical expenses reimbursement to to 1,000,000</div>
                        <div>Personal assistance abroad up to 5,000</div>
                        <div>Travel assistance abroad up to 1,000 per insured per travel</div>
                        <div>Coverage duration: 1 year</div>
                        <button onClick={() => this.selectPlan("universe")}>{ plan === "universe"?<Fragment> <img src={checkIcon} /> <span>Plan Selected</span> </Fragment>: "Choose Plan"}</button>
                    </div>

                </div>

            </div>

        </div>)
    }
}

const mapStateToProps = (state) => ({
    quote: state.quote.quote
})
export default connect(mapStateToProps)(PlanComponent);

