import React, { Component } from 'react'
import { connect } from 'react-redux';
import { calculateQuote } from '../../actions/quote';
import styles from './quote.component.scss'

class QuoteComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            age: 0,
            price: 0,
            car: "",
            submitted: false,
            loading: false,
            globalPrice: null,
            universalPrice: null,
            error: ''
        };
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { age, price, car } = this.state;
        if(price < 5){
            this.setState({
                "error": "Sorry! The price of the car is too low"
            })
            return
        }else if(age < 18){
            this.setState({
                error: "Sorry! The driver is too young"
            })
            return
        }
        else if (age < 25 && car === "Porche"){
            this.setState({
                error: "Sorry! We can not accept this particular risk"
            })
            return

        }

        let globalPrice, universalPrice

        switch (car) {
            case "Audi":
                globalPrice = 250;
                universalPrice = 250 + (.3 * price)
                break;
            case "BMW":
                globalPrice = 150;
                universalPrice = 150 + (.4 * price)
                break;
            case "Porche":
                globalPrice = 500;
                universalPrice = 500 + (.7 * price)
                break;

        }

        this.props.calculateQuote({
            globalPrice,
            universalPrice
        })


       this.props.history.push("/plan")

    }

    render() {
        if(!this.props.username){
           //this.props.history.push("/")
        }
        const { age, price, car, error } = this.state

        return (
            <div className="card main white">
            <div className="row">
                {/*<form className="col s12" onSubmit={this.handleSubmit}>*/}
                    <form className="col s6 offset-s3" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col s6 ">
                            <span>Age of driver :</span>
                        </div>
                        <div className="input-field col s6">
                            <input id="age" name="age" type="number" className={age && age < 18? "error validate": "validate"}
                                   value={age} onChange={this.handleChange}/>

                        </div>
                    </div>

                    <div className="row">
                        <div className="col s6 ">
                            <span> Car :</span>
                        </div>
                        <div className="input-field col s6 ">
                            <select
                            className="browser-default"
                            name={"car"}
                            onChange={this.handleChange}
                            value={car}
                            >
                            <option value="" disabled >Choose your
                                option
                            </option>
                            <option value="Audi" >Audi</option>
                            <option value="BMW" >BMW</option>
                            <option value="Porche" >Porche</option>
                        </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s6">
                            <span> Purchase Price</span>
                        </div>
                        <div className="input-field col s6 ">
                            <input id="price"
                                   type="number"
                                   name="price"
                                   className={price && price < 5? "error validate": "validate"}
                                   value={price}
                                   onChange={this.handleChange}/>
                        </div>
                    </div>
                    {error &&
                    <div className={'error'}>{error}</div>
                    }
                    <button disabled={!(age && price && car)} className={`btn ${styles.button}`}>Get Price</button>
                </form>
            </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    calculateQuote: (quote) => dispatch(calculateQuote(quote))

})

const mapStateToProps = (state) => ({
    username: state.user.username
})
export default connect(mapStateToProps, mapDispatchToProps)(QuoteComponent);