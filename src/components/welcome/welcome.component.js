import React, {Component} from 'react'
import styles from './welcome.module.scss'
import FormComponent from '../loginForm/form.component.js';

export class WelcomeComponent extends Component {

    render() {
        return (
            <div className={styles.welcome}>
                <nav className={styles.nav}>
                    <div className="nav-wrapper"></div>
                </nav>
                <div className={styles.logo}></div>
                <FormComponent {...this.props}/>
            </div>
        );
    }}

export default WelcomeComponent;