import React, {Component} from 'react'
import { connect } from 'react-redux';
import { login } from '../../actions/user';
import styles from './form.module.scss'

export class FormComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitted: false,
            loading: false,
            error: ''
        };
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password  } = this.state;

        // stop here if loginForm is invalid
        if (!(username && password)) {
            return;
        }
        if(username ==="Qover" && password === 'Ninja'){
            this.setState({ loading: true });
            this.props.login(username);
            this.props.history.push("/quote");
        }else {
            this.setState({
                error: "Invalid login credentials"
            })

        }

    }

    render() {
        const { username, password, submitted, error, loading } = this.state;
        return (
            <div className="row">
                <div className="col s12 m6">
                    <div className={`card white ${styles.signup}`}>
                        <div className="card-content">
                            <span className={`card-title ${styles.title}`}>Welcome at Qover</span>
                            <form className="col s12" name="form" onSubmit={this.handleSubmit}>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id="username" type="text" className={"validate" + (submitted && !username) ? ' has-error' : ''} name="username" value={username} onChange={this.handleChange}  />
                                        <label className="active" htmlFor="email">Email</label>
                                        {submitted && !username &&
                                        <div className={`help-block ${styles.errormessage}`}>Email is required</div>
                                        }
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id="password" type="password" className={"validate"+ (submitted && !password) ? ' has-error' : ''} name="password" value={password} onChange={this.handleChange}/>
                                        <label htmlFor="password">Password</label>
                                        {submitted && !password &&
                                        <div className={`help-block ${styles.errormessage}`}>Password is required</div>
                                        }
                                    </div>
                                </div>
                                <p className={styles.action}>
                                    <a href="#">Remember me</a>
                                    <a href="#">Forgot your Password?</a>
                                </p>
                                <button disabled={loading} className={`btn ${styles.button}`}>Sign in to your account</button>
                                {error &&
                                <div className={'alert alert-danger'}>{error}</div>
                                }
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }}

const mapDispatchToProps = (dispatch) => ({
    login: (username) => dispatch(login(username))
})

export default connect(null , mapDispatchToProps)(FormComponent);