import actionTypes from '../actions'

export default (state={}, action) => {
    switch (action.type){
        case actionTypes.LOGIN_SUCCESS:
            return { ...state, isLoggedIn: true, username: action.payload};
        case actionTypes.LOGOUT:
            return { ...state, isLoggedIn: false, username: "" }
        default:
            return state
    }
}