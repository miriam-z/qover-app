import { combineReducers } from 'redux';
import user from './user';
import quote from './quote'


export  default combineReducers({
    user,
    quote
})
