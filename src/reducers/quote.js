import actionTypes from '../actions'

export default (state={}, action) => {
    switch (action.type){
        case actionTypes.CAL_QUOTE_SUCCESS:
            return { ...state, quote: action.payload};
        default:
            return state
    }
}